function G = ComputeStageCosts( stateSpace, controlSpace, map, gate, mansion, cameras )
%COMPUTESTAGECOSTS Compute stage costs.
% 	Compute the stage costs for all states in the state space for all
%   control inputs.
%
%   G = ComputeStageCosts(stateSpace, controlSpace, map, gate, mansion,
%   cameras) computes the stage costs for all states in the state space
%   for all control inputs.
%
%   Input arguments:
%
%       stateSpace:
%           A (K x 2)-matrix, where the i-th row represents the i-th
%           element of the state space.
%
%       controlSpace:
%           A (L x 1)-matrix, where the l-th row represents the l-th
%           element of the control space.
%
%       map:
%           A (M x N)-matrix describing the terrain of the estate map.
%           Positive values indicate cells that are inaccessible (e.g.
%           trees, bushes or the mansion) and negative values indicate
%           ponds or pools.
%
%   	gate:
%          	A (2 x 1)-matrix describing the position of the gate.
%
%    	mansion:
%          	A (F x 2)-matrix indicating the position of the cells of the
%           mansion.
%
%    	cameras:
%          	A (H x 2)-matrix indicating the positions of the cameras.
%
%   Output arguments:
%
%       G:
%           A (K x L)-matrix containing the stage costs of all states in
%           the state space for all control inputs. The entry G(i, l)
%           represents the cost if we are in state i and apply control
%           input l.

% put your code here

    P = computeTP( stateSpace, controlSpace, map, gate, mansion, cameras );
    K = size(stateSpace, 1);
    L = size(controlSpace, 1);
    
    G = inf*ones(K, L);

    kGate = getLinIndex(gate, stateSpace);
    moves = [0 1; -1 0; 0 -1; 1 0; 0 0];
    for i = 1:K
        state = stateSpace(i, :);
        linState = getLinIndex(state, stateSpace);
        for j = 1:L
            nState = state + moves(j, :);          %next state
            nStateLin = getLinIndex(nState, stateSpace);
            if isValid(nState, map)
                cost = 1;
                if (j ~= L && map(nState(2), nState(1)) < 0)
                    %Paparazi moves to pond, cost is 4 but if stays in pond, cost is 1 not 4
                    cost = 4;
                end
                if (nStateLin == kGate)
                    if(j == L)
                        %if Paparazi is taking photo, we must constider he
                        %might win
                        G(i,j) = cost + 8*(pSeen(gate, cameras, map) * (1 - pWin(gate,mansion,map)));
                    else
                        G(i,j) = cost + 8*(pSeen(gate, cameras, map));
                    end
                else
                    G(i,j) = cost + 8*P(i, kGate, j);
                end
            end
        end
    end

end



function [ P ] = computeTP( stateSpace, controlSpace, map, gate, mansion, cameras )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    K = size(stateSpace, 1);
    L = size(controlSpace, 1);
    
    pS = pSeen(stateSpace, cameras, map);
    pW = pWin(stateSpace, mansion, map);
    PN = ones(K, K, L);
    kGate = getLinIndex(gate, stateSpace);
    
    moves = [0 1; -1 0; 0 -1; 1 0];
    
    for i = 1:K
        state = stateSpace(i, :);
        linState = getLinIndex(state, stateSpace);
        sPS = pS(linState);  %probability to be seen in current state
        for j = 1:L-1
            nState = state + moves(j, :);          %next state
            if ~isValid(nState, map)
                if (linState == kGate)
                    PN(i, linState, j) = 0;
                else
                    PN(i, i, j) = PN(i, i, j) * sPS;
                    PN(i, kGate, j) = PN(i, kGate, j) * (1 - sPS);
                end
            else
                nStateLin = getLinIndex(nState, stateSpace);
                nPS    = pS(nStateLin, :);         %probability to be seen in next state
                %% check if current state is pond
                if map(nState(2), nState(1)) < 0
                    nPS = 1- (1 - nPS)^4;
                end
                if (nStateLin == kGate)
                    PN(i, nStateLin, j) = 0;
                else
                    PN(i, nStateLin, j) = PN(i, nStateLin, j) * nPS;
                    PN(i, kGate, j) = PN(i, kGate, j)*(1 - nPS);
                end
            end
        end
        % compute for photo
        if (linState == kGate)
            PN(i, i, L) = pW(linState);
        else
            PN(i, i, L) = PN(i, i, L) * (1 - (1 - sPS) * (1-pW(linState)));  % probability to not be seen and not to win
            PN(i, kGate, L) = PN(i, kGate, L) * (1 - sPS * (1-pW(linState)));    % probability to be seen and not to win
        end
    end
    P = 1 - PN;

end


function [ k ] = getLinIndex( state, stateSpace )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
k = find(stateSpace(:, 1) == state(1) & stateSpace(:, 2) == state(2));

%k = a(a==b);

end


function [ valid ] = isValid( state, map)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    valid = true;
    [M, N] = size(map);
    if state(1) > N || state(1) < 1 || state(2) > M || state(2) < 1
        valid = false;
        return;
    end
    if map(state(2),state(1)) > 0;
        valid = false;
        return;
    end

        
end

function [ pSeen ] = pSeen( states, cameras, map)
%UNTITLED2 Summary of this function goes here
%   states(R x 2) (R = number of states to check)

    pNseen = ones(size(states,1),1);
    for i = 1:size(states, 1)
        %% check all cameras with same m
        mdx = find(cameras(:,1) == states(i, 1));
        mapRow = map(:,states(i, 1));
        n_occlusion = find(mapRow > 0);                             %get all occlusions from map with same m
        
        n_occlusion1 = n_occlusion(n_occlusion > states(i, 2));      %occluisons to one side
        n_occlusion2 = n_occlusion(n_occlusion < states(i, 2));      %occluisons to other side
        [ ~, n_I1] = min(n_occlusion1 - states(i, 2));
        n_oc1 = n_occlusion1(n_I1);                                 %closest occlusion to one side
        if isempty(n_oc1)
            n_oc1 = inf;
        end
        [ ~, n_I2] = min(states(i, 2) - n_occlusion2);
        n_oc2 = n_occlusion2(n_I2);                                 %closest occlusion to other side
        if isempty(n_oc2)
            n_oc2 = -inf;
        end
        
        for j = 1:length(mdx)
            if (cameras(mdx(j), 2) <= n_oc1) && (cameras(mdx(j), 2) >= n_oc2)     %check if occlusion in the way of camera
                pNseen(i) = pNseen(i) * (1 - cameras(mdx(j), 3)/abs(cameras(mdx(j), 2) - states(i, 2)));
            end
        end
        
        %% check all cameras with same n
        ndx = find(cameras(:,2) == states(i, 2));
        mapColumn = map(states(i, 2),:);
        m_occlusion = find(mapColumn > 0);                          %get all occlusions from map with same n
        
        m_occlusion1 = m_occlusion(m_occlusion > states(i, 1));      %occluisons to one side
        m_occlusion2 = m_occlusion(m_occlusion < states(i, 1));      %occluisons to other side
        [ ~, m_I1] = min(m_occlusion1 - states(i, 1));
        m_oc1 = m_occlusion1(m_I1);                                 %closest occlusion to one side
        if isempty(m_oc1)
            m_oc1 = inf;
        end
        [ ~, m_I2] = min(states(i, 1) - m_occlusion2);
        m_oc2 = m_occlusion2(m_I2);                                 %closest occlusion to other side
        if isempty(m_oc2)
            m_oc2 = -inf;
        end
        for j = 1:length(ndx)
            if cameras(ndx(j), 1) <= m_oc1 && cameras(ndx(j), 1) >= m_oc2     %check if occlusion in the way of camera
                pNseen(i) = pNseen(i) * (1 - cameras(ndx(j), 3)/abs(cameras(ndx(j), 1) - states(i, 1)));
            end
        end
    end
    
    pSeen = 1 - pNseen;
end


function [ PW ] = pWin( states, mansion, map)
%UNTITLED2 Summary of this function goes here
    pNotWin = ones(size(states,1),1);
    global p_c;
    global gamma_p;
    for i = 1:size(states, 1)
        mdx = find(mansion(:,1) == states(i,1));
        mapRow = map(:,states(i, 1));
        n_occlusion = find(mapRow > 0);                             %get all occlusions from map with same m
        
        n_occlusion1 = n_occlusion(n_occlusion > states(i, 2));      %occluisons to one side
        n_occlusion2 = n_occlusion(n_occlusion < states(i, 2));      %occluisons to other side
        
        [ ~, n_I1] = min(n_occlusion1 - states(i, 2));
        n_oc1 = n_occlusion1(n_I1);                                 %closest occlusion to one side
        if isempty(n_oc1)
            n_oc1 = inf;
        end
        
        [ ~, n_I2] = min(states(i, 2) - n_occlusion2);
        n_oc2 = n_occlusion2(n_I2);                                 %closest occlusion to other side
        if isempty(n_oc2)
            n_oc2 = -inf;
        end 
        
        for j = 1:length(mdx)
            if (mansion(mdx(j), 2) <= n_oc1) && (mansion(mdx(j), 2) >= n_oc2)     %check if occlusion in the way of camera
                pNotWin(i) = pNotWin(i) * (1 - gamma_p/abs(mansion(mdx(j), 2) - states(i, 2)));
            end
        end
        
        %% check all mansion with same n
        ndx = find(mansion(:,2) == states(i, 2));
        mapColumn = map(states(i, 2),:);
        m_occlusion = find(mapColumn > 0);                          %get all occlusions from map with same n
        
        m_occlusion1 = m_occlusion(m_occlusion > states(i, 1));      %occluisons to one side
        m_occlusion2 = m_occlusion(m_occlusion < states(i, 1));      %occluisons to other side
        
        [ ~, m_I1] = min(m_occlusion1 - states(i, 1));
        m_oc1 = m_occlusion1(m_I1);                                 %closest occlusion to one side
        if isempty(m_oc1)
            m_oc1 = inf;
        end
        
        [ ~, m_I2] = min(states(i, 1) - m_occlusion2);
        m_oc2 = m_occlusion2(m_I2);                                 %closest occlusion to other side
        if isempty(m_oc2)
            m_oc2 = -inf;
        end
        for j = 1:length(ndx)
            if mansion(ndx(j), 1) <= m_oc1 && mansion(ndx(j), 1) >= m_oc2     %check if occlusion in the way of mansion
                pNotWin(i) = pNotWin(i) * (1 - gamma_p/abs(mansion(ndx(j), 1) - states(i, 1)));
            end
        end
    end
    PW = max((1 - pNotWin), p_c);
    
end