function [ pSeen ] = pSeen( states, cameras, map)
%UNTITLED2 Summary of this function goes here
%   states(R x 2) (R = number of states to check)

    pNseen = ones(size(states,1),1);
    for i = 1:size(states, 1)
        %% check if current state is pond
        EXP = 1;
        if map(states(i, 2), states(i, 1)) < 0
            EXP = 4;
        end
        %% check all cameras with same m
        mdx = find(cameras(:,1) == states(i, 1));
        mapRow = map(:,states(i, 1));
        n_occlusion = find(mapRow > 0);                             %get all occlusions from map with same m
        
        n_occlusion1 = n_occlusion(n_occlusion > states(i, 2));      %occluisons to one side
        n_occlusion2 = n_occlusion(n_occlusion < states(i, 2));      %occluisons to other side
        [ ~, n_I1] = min(n_occlusion1 - states(i, 2));
        n_oc1 = n_occlusion1(n_I1);                                 %closest occlusion to one side
        if isempty(n_oc1)
            n_oc1 = inf;
        end
        [ ~, n_I2] = min(states(i, 2) - n_occlusion2);
        n_oc2 = n_occlusion2(n_I2);                                 %closest occlusion to other side
        if isempty(n_oc2)
            n_oc2 = -inf;
        end
        
        for j = 1:length(mdx)
            if (cameras(mdx(j), 2) <= n_oc1) && (cameras(mdx(j), 2) >= n_oc2)     %check if occlusion in the way of camera
                pNseen(i) = pNseen(i) * (1 - cameras(mdx(j), 3)/abs(cameras(mdx(j), 2) - states(i, 2)))^EXP;
            end
        end
        
        %% check all cameras with same n
        ndx = find(cameras(:,2) == states(i, 2));
        mapColumn = map(states(i, 2),:);
        m_occlusion = find(mapColumn > 0);                          %get all occlusions from map with same n
        
        m_occlusion1 = m_occlusion(m_occlusion > states(i, 1));      %occluisons to one side
        m_occlusion2 = m_occlusion(m_occlusion < states(i, 1));      %occluisons to other side
        [ ~, m_I1] = min(m_occlusion1 - states(i, 1));
        m_oc1 = m_occlusion1(m_I1);                                 %closest occlusion to one side
        if isempty(m_oc1)
            m_oc1 = inf;
        end
        [ ~, m_I2] = min(states(i, 1) - m_occlusion2);
        m_oc2 = m_occlusion2(m_I2);                                 %closest occlusion to other side
        if isempty(m_oc2)
            m_oc2 = -inf;
        end
        for j = 1:length(ndx)
            if cameras(ndx(j), 1) <= m_oc1 && cameras(ndx(j), 1) >= m_oc2     %check if occlusion in the way of camera
                pNseen(i) = pNseen(i) * (1 - cameras(ndx(j), 3)/abs(cameras(ndx(j), 1) - states(i, 1)))^EXP;
            end
        end
    end
    
    pSeen = 1 - pNseen;
end

