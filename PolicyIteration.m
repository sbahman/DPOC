function [ J_opt, u_opt_ind ] = PolicyIteration( P, G )
%POLICYITERATION Value iteration
%   Solve a stochastic shortest path problem by Policy Iteration.
%
%   [J_opt, u_opt_ind] = PolicyIteration(P, G) computes the optimal cost and
%   the optimal control input for each state of the state space.
%
%   Input arguments:
%
%       P:
%           A (K x K x L)-matrix containing the transition probabilities
%           between all states in the state space for all control inputs.
%           The entry P(i, j, l) represents the transition probability
%           from state i to state j if control input l is applied.
%
%       G:
%           A (K x L)-matrix containing the stage costs of all states in
%           the state space for all control inputs. The entry G(i, l)
%           represents the cost if we are in state i and apply control
%           input l.
%
%   Output arguments:
%
%       J_opt:
%       	A (K x 1)-matrix containing the optimal cost-to-go for each
%       	element of the state space.
%
%       u_opt_ind:
%       	A (K x 1)-matrix containing the index of the optimal control
%       	input for each element of the state space.

% put your code here
    [K, L] = size(G);

    Jk = zeros(K,1);
    J_opt = ones(K,1);
    u_opt_ind = 5*ones(K,1);
    
    A = zeros(K,K);
    g = zeros(K,1);
    
    JL = zeros(K,L);
    
    thresh = 0.0000001;
    while(max(abs(Jk - J_opt)) > thresh)
        Jk = J_opt;
        for i = 1:K
            g(i) = G(i, u_opt_ind(i));
            A(i,:) = P(i, :, u_opt_ind(i))';
        end
        A = eye(K) - A;
        
        J_opt = A\g;
        
        for i = 1:K
            for u = 1:L
                JL(i,u) = G(i,u);
                for j = 1:K
                    JL(i,u) = JL(i,u) + P(i, j, u)*J_opt(j);
                end
            end
        end
        [~, u_opt_ind] = min(JL, [], 2);
        disp(max(abs(Jk - J_opt)));
    end


end

