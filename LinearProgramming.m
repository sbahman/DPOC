function [ J_opt, u_opt_ind ] = LinearProgramming( P, G )
%LINEARPROGRAMMING Value iteration
%   Solve a stochastic shortest path problem by Linear Programming.
%
%   [J_opt, u_opt_ind] = LinearProgramming(P, G) computes the optimal cost
%   and the optimal control input for each state of the state space.
%
%   Input arguments:
%
%       P:
%           A (K x K x L)-matrix containing the transition probabilities
%           between all states in the state space for all control inputs.
%           The entry P(i, j, l) represents the transition probability
%           from state i to state j if control input l is applied.
%
%       G:
%           A (K x L)-matrix containing the stage costs of all states in
%           the state space for all control inputs. The entry G(i, l)
%           represents the cost if we are in state i and apply control
%           input l.
%
%   Output arguments:
%
%       J_opt:
%       	A (K x 1)-matrix containing the optimal cost-to-go for each
%       	element of the state space.
%
%       u_opt_ind:
%       	A (K x 1)-matrix containing the index of the optimal control
%       	input for each element of the state space.

% put your code here
    [K, L] = size(G);
    
    H = reshape(G, [K*L,1]);
    %H(H ==inf ) = 10^6;
    A = [];
    for i = 1:L
        A = [A; (eye(K) - P(:,:,i))];
    end
    idinf = find(H == inf);
    H(idinf) = [];
    A(idinf,:) = [];
    J_opt = linprog(-ones(K,1), A, H);
    
    u_opt_ind = zeros(K,1);
    
    for i = 1:L
        u_opt_ind(abs(J_opt - (G(:,i) + P(:,:,i)*J_opt)) < 0.00001) = i;
    end
    
end

