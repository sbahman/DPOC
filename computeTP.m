function [ P ] = computeTP( stateSpace, controlSpace, map, gate, mansion, cameras )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    K = size(stateSpace, 1);
    L = size(controlSpace, 1);
    
    pS = pSeen(stateSpace, cameras, map);
    pW = pWin(stateSpace, mansion, map);
    PN = ones(K, K, L);
    kGate = getLinIndex(gate, stateSpace);
    
    moves = [0 1; -1 0; 0 -1; 1 0];
    
    for i = 1:K
        state = stateSpace(i, :);
        linState = getLinIndex(state, stateSpace);
        sPS = pS(linState);  %probability to be seen in current state
        for j = 1:L-1
            nState = state + moves(j, :);          %next state
            if ~isValid(nState, map)
                if (linState == kGate)
                    PN(i, linState, j) = 0;
                else
                    PN(i, i, j) = PN(i, i, j) * sPS;
                    PN(i, kGate, j) = PN(i, kGate, j) * (1 - sPS);
                end
            else
                nStateLin = getLinIndex(nState, stateSpace);
                nPS    = pS(nStateLin, :);         %probability to be seen in next state
                if (nStateLin == kGate)
                    PN(i, nStateLin, j) = 0;
                else
                    PN(i, nStateLin, j) = PN(i, nStateLin, j) * nPS;
                    PN(i, kGate, j) = PN(i, kGate, j)*(1 - nPS);
                end
            end
        end
        % compute for photo
        if (linState == kGate)
            PN(i, i, L) = pW(linState);
        else
            PN(i, i, L) = PN(i, i, L) * (1 - (1 - sPS) * (1-pW(linState)));  % probability to not be seen and not to win
            PN(i, kGate, L) = PN(i, kGate, L) * (1 - sPS * (1-pW(linState)));    % probability to be seen and not to win
        end
    end
    P = 1 - PN;

end

